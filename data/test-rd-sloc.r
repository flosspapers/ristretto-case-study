#!/usr/bin/r

ristretto <- read.csv(file = "ristretto.csv", sep = ";")

correlation = cor.test(ristretto$RD, ristretto$SLOC, alternative = "greater")

cat("Correlation = ", correlation$estimate, "\n", sep = "")
cat("p = ", correlation$p.value, "\n", sep = "")
