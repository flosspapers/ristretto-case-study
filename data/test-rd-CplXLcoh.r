#!/usr/bin/r

ristretto <- read.csv(file = "ristretto.csv", sep = ";")

correlation <- cor.test(ristretto$RD, ristretto$CplXLcoh_LCOM1, alternative = "greater")
cat("Using LCOM1\n", "============\n", sep = "")
cat("Correlation = ", correlation$estimate, "\n", sep = "")
cat("p = ", correlation$p.value, "\n", sep = "")

correlation <- cor.test(ristretto$RD, ristretto$CplXLcoh_LCOM4, alternative = "greater")
cat("Using LCOM4\n", "============\n", sep = "")
cat("Correlation = ", correlation$estimate, "\n", sep = "")
cat("p = ", correlation$p.value, "\n", sep = "")
