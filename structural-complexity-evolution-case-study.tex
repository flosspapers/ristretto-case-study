\documentclass[ifip]{svmult}

\usepackage[noamsthm]{beamerarticle}
\usepackage[]{url}
\usepackage[]{graphicx}
\usepackage{subfigure}
\usepackage[utf8]{inputenc}

\title[Structural Complexity Evolution in FS projects]{Structural Complexity Evolution in Free Software Projects: A
Case Study}
\titlerunning{Structural Complexity Evolution in FS Projects: A Case Study}
\author{Antonio Terceiro \and Christina Chavez}
\institute{
  Computer Science Department --
  Universidade Federal da Bahia\\
  \texttt{\{terceiro,flach\}@dcc.ufba.br}
}
\mode<presentation>{
\date{QACOS-OSSPL Workshop -- June 6th, 2009\\Skövde, Sweden}
}

\mode<presentation>{
\AtBeginSection[]
{
  \begin{frame}<beamer>{Outline}
    \tableofcontents[currentsection,hideallsubsections]
  \end{frame}
}
}

\begin{document}

\begin{frame}[plain]
\maketitle
\end{frame}

\mode<presentation>{
\begin{frame}
  \frametitle{Outline}
  \tableofcontents[hideallsubsections]
\end{frame}
}

\begin{abstract}
  % context
  The fundamental role of Free Software in contemporary IT Industry is
  sometimes threatened by the lack of understanding of its development
  process, which sometimes leads to distrust regarding quality of Free
  Software projects.
  % problem
  While Free Software projects do have quality assurance
  activities, there is still room for improvement and introduction of
  new methods and tools.
  % solution
  This paper presents a case study of structural complexity evolution in
  a small project written in C. The approach used can be used by Free
  Software developers in their own projects to understand and control
  complexity, and the results provide initial understanding on the study
  of structural complexity evolution.
\end{abstract}

\section{Introduction}

Free Software\footnote{also referred to in the research community as
``Open Source Software'' (OSS), ``Free/Open Source Software'' (FOSS),
``Free/Libre/Open Source Software'' (FLOSS) etc.} plays a fundamental
role in the IT Industry in the contemporary society. Companies,
governments and non-profit organizations recognize the potential of Free
Software and are at least considering it. But Free Software is developed
in a way too much different from the ``conventional'' software these
organizations are used to: teams are distributed throughout the world,
most of the time with no contractual obligations; normally, there are no
formal requirements specifications, in the sense of those we expect in
traditional software development organizations; quality assurance does
happen, while not in the way an outsider expects (and perhaps it's not
as good as it could be). Quoting Scacchi
\cite{scacchi:research-results-and-opportunities-2007}, Free Software
Development is not Software Engineering done poorly, it's just
different.

While there is quality assurance (QA) activities in Free Software projects
\cite{zhao2003}, there is a
lot of room for improvement in this area. One of the areas in which Free
Software QA can advance is at the use of project metrics to inform,
guide and control development. It's not uncommon to find projects in
which the lead developer(s) lost the interest and there are
users/contributors willing to continue the development, but the
complexity of the code makes it more practical for them to start a new
project as a replacement to the original. Sometimes the lead
developer(s) realize
that the code became so complex that it's more cost-effective for them
to rewrite large parts of the software, or even to rewrite it entirely
from scratch, than investing time in enhancing existing code.

So, if Free Software developers have tools and methods to understand and
tame the complexity of their code, there will be a more healthy Free
Software ecosystem. Less complex code may promote
maintainability and help Free Software projects to get and retain new
contributors.

\mode<presentation>{
\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
    \item Free Software plays an important role in today's IT industry.
    \item Free Software ``development is not Software Engineering done
      poorly, it's just different
      \cite{scacchi:research-results-and-opportunities-2007}
    \item While there is QA in FS projects \cite{zhao2003}, there is much room for
      improvement in that area.
  \end{itemize}
\end{frame}
}

This paper goals are twofold: first, to experiment an approach for
studying the evolution of structural complexity in Free Software
projects that can be used by Free Software developers in their own
projects; second, to provide initial results on the study of structural
complexity evolution in Free Software projects written in C (so we can
e.g. compare them with the Java projects studied in
\cite{stewart-et-al-2006}). For that, we present a case study in which
we analyze the evolution of structural complexity in a small Free
Software project during a period of approximately 15 months.

\mode<presentation>{
\begin{frame}
  \frametitle{Goals of this study}
  \begin{enumerate}
    \item Experiment an approach for studying the evolution of
      Structural Complexity in Free Software Projects
      \structure{that would be used by Free Software developers}.
    \item Provide initial results on the study of Structural Complexity
      evolution in C projects (willing to compare with the Java ones
      already reported \cite{stewart-et-al-2006}).
  \end{enumerate}
\end{frame}
}

The remainder of this paper is organized as follows:
%
related work is described in section \ref{sec:related-work};
%
section \ref{sec:egypt} briefly describes the tool infrastructure used to
extract structural complexity metrics from projects written in the C
language;
%
section \ref{sec:case-study} presents the case study;
%
and finally, we provide final remarks and discuss future work in section
\ref{sec:conclusion}.

\section{Related Work}
\label{sec:related-work}

Stewart and colleagues studied 59 projects written in Java
that were available on Sourceforge
\cite{stewart-et-al-2006}, using as a software complexity measure the
metric ``CplXLCoh'', the product of Coupling (``Cpl'') and Lack of
Cohesion (``LCoh'').
%
They found four patterns on software complexity evolution among those
projects:
1) early decreasers, in which the complexity starts to decrease
in the very beginning of the project's public availability, then gets
stable for a period, and then starts a slight growing trend;
2) early increasers, where the complexity starts to increase just during
the beginning of the project, and after some time gets stable;
3) midterm increasers, which experiences a faster growing of the
complexity several months after the start of the project; and
4) midterm decreasers, that continued to decrease complexity during the
middle of the observed period before stabilizing.

Capiluppi and Boldyreff \cite{capiluppi2007b} presented an approach to use
coupling information to indicate potentially reusable parts of projects, which
could be distributed as independent projects and reused by other software in
the same application domain or with similar non-functional requirements.
Their approach was based on a instability metric, as in the work of
Martin \cite{martin2003}. This metric is defined in terms of
afferent coupling (number of modules calling the module in question,
$C_a$) and efferent coupling (number of modules that the module in
question calls, $C_e$), as $I = \frac{C_e}{C_a + C_e}$. They show
that modules (represented in their study by folders) with low
instability (i.e. stable modules) are good candidates to be turned into
independent, external modules.

Wu \cite{wu2006} studied the dynamics of Free Software projects
evolution, and argues that it happens in the form of punctuated
equilibrium: the projects alternate between periods of localized and
incremental changes and periods with deep architectural changes.

\mode<presentation>{
\begin{frame}
  \frametitle{Related Work}
  \begin{itemize}
    \item Stewart et al studied Structural Complexity Evolution in Java
      projects. \cite{stewart-et-al-2006}
    \item Capiluppi and Boldyreff presented an approach to identify
      potentially reusable parts in projects using coupling information.
      \cite{capiluppi2007b}
    \item Wu describes Free Software projects evolution as
      \emph{punctuated equilibrium}.
      \cite{wu2006}
  \end{itemize}
\end{frame}
}

\section[egypt: tool support]{\texttt{egypt}: tool support for extracting coupling and cohesion data
from C programs}
\label{sec:egypt}

% describe egypt briefly
\texttt{egypt} is program originally developed by Andreas
Gustafsson\footnote{\url{http://www.gson.org/egypt/}}. It works by
reading intermediate files generated by the GNU C Compiler and producing
as output a call graph in the format used by the Graphviz graph
visualization software \footnote{\url{http://www.graphviz.org/}}, so we
can visualize the call dependencies between functions in C source code.

We made the following main modifications in \texttt{egypt} to use in
this study:

\begin{itemize}
  \item Implementation of variables usage detection, to identify which
    functions use which variables. This is used for calculating both
    coupling between modules (in the case where modules use variables
    from other modules) and lack of cohesion for a given module.
  \item Addition of an option to group the calls and variable usages by module,
    so that we can have a module dependency view. The original \texttt{egypt}
    only produced graphs at the function level, which makes it
    impossible to understand the structure of non-trivial software.
  \item Refactoring of the \texttt{egypt} script into an object-oriented
    design to be able to plug different extraction and reporting
    modules.
  \item Implementation of a metrics output which, instead of producing
    Graphviz input files, produces a metrics report on the extracted
    design including coupling and cohesion data.
\end{itemize}

Our modified version is available as a git repository at
\url{http://github.com/terceiro/egypt}.

Since \texttt{egypt} extracts coupling data, it can also be used to carry
studies like the one by Capiluppi and Boldyreff \cite{capiluppi2007b} to
identify potentially reusable modules in Free Software Projects. This is
not our interest in this paper, though.

\mode<presentation>{
\begin{frame}
  \frametitle{egypt}
  \begin{itemize}
    \item Originally developed by Andreas Gustafsson.
    \item Reads intermediate files left by the GNU C Compiler and
      generates function call graphs.
    \item We implemented several enhancements.
    \item Currently available at
      \url{http://github.com/terceiro/egypt}.
\end{itemize}
\end{frame}
}

\section{Case study: the Ristretto project}
\label{sec:case-study}

Ristretto\footnote{\url{http://goodies.xfce.org/projects/applications/ristretto}}
is a fast and lightweight picture-viewer for the Xfce desktop
environment. It's written in C, uses the GTK+ user interface toolkit and
is licensed under the GNU General Public License, version 2 or later.
%\subsection{Study definition}
In this study we
\textbf{analyze} the Ristretto project
\textbf{with the goal of} characterizing it
\textbf{with respect to} size and complexity over time
\textbf{from the perspective} of developers.

\subsection{Planning}

In this study, our ``population'' is the series of releases the
Ristretto project had since the beginning of its development. This
includes 21 consecutive releases, from 0.0.1 to 0.0.21, spanning a
period of approximately 15 months. 

\mode<presentation>{
\begin{frame}
  \frametitle{The Ristretto project}
  \begin{itemize}
    \item Xfce's picture viewer.
    \item written in C, uses GTK+, licensed under GPLv2+.
    \item 21 releases, spanning a period of 15 months.
  \end{itemize}
\end{frame}
}

For each release of the project, the following data was extracted:

\begin{frame}
  \frametitle<presentation>{Variables}
  \begin{itemize}
    \item Independent variables:
      \begin{itemize}
          %\item Release serial number ($RSN$): a number indicating the
          %ordering between the releases. The first release has $RSN = 1$,
          %the second has $RSN = 2$, and so on.
          \item Release day ($RD$): the number of days that has passed since
            the first release. The first release itself has $RD = 0$.
        \end{itemize}
      \item Dependent variables:
        \begin{itemize}
          \item Physical Source Lines of Code ($SLOC$).
            %\item Number of modules ($NM$).
            \item The product of average module coupling and average module
              lack of cohesion ($CplXLCoh$), as in \cite{stewart-et-al-2006},
              as a measure of complexity. We have used the classic coupling
              and lack of cohesion metrics by Chidamber and Kemerer
              \cite{chidamber1994}.
          \end{itemize}
      \end{itemize}
\end{frame}

We formulated two hypothesis for this study, which are described below.

\paragraph{Hypothesis 1.}
We want to test whether the project presents a consistent growth, as
reported in the literature for both ``conventional'' Software
Engineering \cite{lehman1997} and for Free Software projects
\cite{koch2007}. 
%
Since we are interested only in testing for consistent growth and don't
need a precise prediction of project size, we consider enough to test
for a linear correlation between the date of release and the size of the
project. Our null hypothesis $H^1_0$ is that there is no linear
correlation between time and size of the project, and our alternative
hypothesis $H^1_A$ stands for a positive linear correlation between the
variables:

\begin{frame}
  \mode<presentation>{
    \frametitle{Hypothesis 1: size}
    There is a positive correlation between $RD$ and $SLOC$.
  }
  \begin{eqnarray*}
    H^1_0:  r_{RD,SLOC} = 0
    & \hspace{1.5cm} &
    H^1_A:  r_{RD,SLOC} > 0
  \end{eqnarray*}
\end{frame}

\paragraph{Hypothesis 2.}
To test whether the project becomes more complex as the time passes, we
want to verify how does our complexity metric evolve.
The theory suggests that software projects tend to become more complex
through time, unless explicit actions are taken to prevent it
\cite{lehman1997}.
%
Our null hypothesis $H^2_0$, then, says there is no linear correlation
between time and complexity; our alternative hypothesis $H^2_A$ is that
there is a positive linear correlation between them:

\begin{frame}
  \mode<presentation>{
    \frametitle{Hypothesis 2: Structural Complexity}
    There is a positive correlation between $RD$ and  $CplXLCoh$.
  }
  \begin{eqnarray*}
    H^2_0: r_{RD,CplXLCoh} = 0
    & \hspace{1.5cm} &
    H^2_A: r_{RD,CplXLCoh} > 0
  \end{eqnarray*}
\end{frame}

\subsection{Data Extraction}

For measuring $SLOC$, we used David A. Wheeler's \texttt{sloccount}
tool, available at \url{http://www.dwheeler.com/sloccount/}. For each
release, \texttt{sloccount} is run and only the total Physical Source
Lines of Code count is taken.

For measuring $CplXLCoh$, we used our modified version of
\texttt{egypt}.

%The metrics report feature gives us both a summary of
%the project, with metrics that are the average of their counterparts in
%each module plus some global counting (number of modules, number of
%functions etc). One of these provided metrics is the average
%``CplXLCoh''.

% \subsection{Operation}

% In order to have the entire history of the project available to extract
% the data, we imported the Ristretto Subversion repository into a
% \texttt{git} \footnote{\url{http://git-scm.org/}} repository using
% \texttt{git}'s Subversion interaction module, \texttt{git-svn}.

% This brought us several benefits:
% 
% \begin{itemize}
%   \item The \texttt{git} repository has the entire history of the
%     project, which can be browsed and queried off-line without imposing
%     extra load on the project Subversion server.
%   \item The \texttt{git} repository allow us to easily access the
%     contents of a given release.
%   \item This local repository, after optimization, has a little more
%     than half the size of a directory with the downloaded packages. The
%     storage space gain is insignificant for this particular study, but
%     will probably be crucial when conducting larger-scale ones.
%   \item This repository can be distributed together with the scripts
%     used to extract the information, thus allowing independent
%     researchers to easily verify and reproduce our results.
% \end{itemize}

% To produce the data used in the study, we used a script that iterates
% through all the releases, gets the release date from the version control
% data, uses \texttt{sloccount} to get the size of the project at that
% given point in time, and invokes \texttt{egypt} to extract the design
% information.

Ristretto's Subversion repository was imported into a
\texttt{git}\footnote{\url{http://www.git-scm.org/}} repository. We then
used a script that iterates through all the releases, gets the release
date from the version control data, checks out the source code of the
given release, invokes \texttt{sloccount} to get the size of the project
at that release, builds the project so the GNU C Compiler generates the
needed intermediate files, and invokes \texttt{egypt} to extract the
design information from the GCC intermediate files.

\mode<presentation>{
\begin{frame}
  \frametitle{Data collection}
  \begin{itemize}
    \item Imported Ristretto's history into a \texttt{git} repository.
    \item checkout each release and:
      \begin{itemize}
        \item count size with \texttt{sloccount}
        \item build and extract module depencency and metrics with \texttt{egypt}
      \end{itemize}
  \end{itemize}
\end{frame}
}

\subsection{Data analysis}

% Size data
Figure \ref{fig:ristretto-size} presents the evolution of size in the
Ristretto project, using the day of release as data in the X axis (while
using the version string as label), and $SLOC$ as the Y-axis. The plot
shows that Ristretto is consistently growing: it goes from approximately
2500 $SLOC$ in the first release to more than 6000 $SLOC$ in the last
observed release. On the other hand, looking at the latest releases
makes us wonder if the growth rate isn't decreasing.

\begin{figure}[htbp]
  \begin{center}
    \subfigure[ristretto-size][Growth of Ristretto project over time]{
      \includegraphics[width=0.45\textwidth]{data/ristretto-size.eps}
      \label{fig:ristretto-size}
    }
    \hfill
    \subfigure[ristretto-complexity][Evolution of the complexity metric in the Ristretto project]{
      \includegraphics[width=0.45\textwidth]{data/ristretto-CplXLcoh_LCOM1.eps}
      \label{fig:ristretto-CplXLcoh-LCOM1}
    }
  \end{center}
  \label{fig:ristretto-evolution-figures}
  \caption{Ristretto evolution data}
\end{figure}

The correlation test, using Pearson's method, gives us $r_{RD,SLOC} =
0.9041602$, with $p < 0.01$. This way we are able to reject the null
hypothesis $H^1_0$ and accept the alternative hypothesis $H^1_A$: the
current data allows us to say that there is a positive linear
correlation between the release day and the size, measured in Physical
Source Lines of Code.

\mode<presentation>{
\begin{frame}
  \frametitle{Size data}
  \begin{figure}[h]
    \begin{center}
      \includegraphics[width=0.65\textwidth]{data/ristretto-size.eps}
    \end{center}
  \end{figure}

  $r_{RD,SLOC} = 0.9041602$, $p < 0.01$. We can accept $H^1_A$.
\end{frame}
}

% Complexity data
Figure \ref{fig:ristretto-CplXLcoh-LCOM1} shows the data for our
complexity metric.
%
The plot shows that although the complexity in increasing, there are
specific releases in which either the complexity does not increase
significantly or even the complexity decreases in comparison with the
previous release. This behavior is discussed in more detail in the
section \ref{sec:interpreting}

The correlation test for $RD$ and $CplXLCoh$ gave us $r_{RD,CplXLCoh} =
0.8636375$ with $p < 0.01$, also using Pearson's method. This way we can
reject our null hypothesis $H^2_0$ and accept our alternative
hypothesis: there is a linear correlation between release day and
complexity.

\mode<presentation>{
\begin{frame}
  \frametitle{Complexity data}
  \begin{figure}[h]
    \begin{center}
      \includegraphics[width=0.65\textwidth]{data/ristretto-CplXLcoh_LCOM1.eps}
    \end{center}
  \end{figure}
  $r_{RD,CplXLCoh} = 0.8636375$ with $p < 0.01$. We can accept $H^2_A$.
\end{frame}
}

\subsection{Interpreting the Results}
\label{sec:interpreting}

% Growth
The growth data allows us to assert that the project is consistently
growing since it's first release. This suggests it's being actively
developed and is receiving new features as an effect of new user
requirements. But growth is not out main interest in this study.

% Complexity data
The complexity data reveals interesting issues. In the long term, the
complexity grows as the time passes, but the curve we could draw through
the data points shows discontinuities (see figure
\ref{fig:ristretto-CplXLcoh-LCOM1}):

\begin{enumerate}
  \item Approximately in the 40th day, in the 6th release of
    Ristretto, the complexity increase is attenuated.
  \item Approximately in the 150th day, in the 16th release, the
    complexity \emph{decreases} in comparison with the previous release.
  \item Approximately in the 450th day, in the 21th and last release,
    the complexity also seems to increase less in comparison with the
    previous release than it increased in the previous release in
    comparison with the release before it.
\end{enumerate}

All these discontinuities coincide with major architectural changes in
Ristretto: releases 0.0.6, 0.0.16 and 0.0.21 introduced new modules in
comparison with their respective previous releases. Figure
\ref{fig:ristretto-design-evolution} shows the four different
architectures Ristretto had during its life cycle: the leftmost graph
shows the architecture of the first release, and them the architecture in
releases 0.0.6, 0.0.16 and 0.0.21.
%The ellipses are modules, and the arrows indicate module dependency.

\begin{frame}
  \frametitle<presentation>{Ristretto's architectural evolution}
  \begin{figure}[htbp]
    \begin{center}
      \includegraphics[width=1\textwidth]{arch-evolution.eps}
    \end{center}
    \caption{
    Ristretto's architectural evolution. Graphs by Graphviz
    from \texttt{egypt} output.
    %Ellipses are modules; an arrow from $A$ to $B$ means that $A$ depends
    %on $B$: functions in $A$ either call functions in $B$ or use variables
    %in $B$. The graphs were automatically generated by Graphviz using
    %\texttt{egypt}'s output.
    }
    \label{fig:ristretto-design-evolution}
  \end{figure}
\end{frame}

It is easy to understand why the introduction of new modules has an
impact in complexity increase. As the newly introduced module tends to
be less complex than the previously existing ones, and the sum of the
complexities is now divided by $n+1$ instead of $n$, the new module
brings down the average complexity metric.

Ristretto exhibits the behavior described by Wu \cite{wu2006}: it
alternates periods of incremental change, in which the complexity
increases, with moments of rupture in which the architecture changes. In
the case of Ristretto, these architecture changes made it less complex,
or at least attenuated the complexity increase trend at that moment.

\mode<presentation>{
 \begin{frame}
   \frametitle{Interpretation}
   \begin{itemize}
     \item Discontinuities in versions 0.0.6, 0.0.16 and 0.0.21.
     \item Addition of new modules bring down the structural complexity
       metric.
     \item Ristretto exhibits the behavior described by Wu
       \cite{wu2006}: it alternates periods of incremental change and
       periods of rupture.
   \end{itemize}
 \end{frame}
}

\subsection{Limitations of this study}

% small project
We chose a small project to study on purpose, since we wanted to do a
exploratory study and experiment the approach we are developing. A
larger project may not exhibit a similar behavior as Ristretto.

% single developer
Although the version control history data lists 13 different
contributors to the Ristretto source folder, we later identified that
actually only one developer made changes to the source code. The other
contributors' changes were mainly updates to user interface
translations, which are separated from the C source code.
%
This way we have no data that allows us to investigate the relationship
between the structural complexity and the number of developers who
contributed in a given period (for example).

\mode<presentation>{
  \begin{frame}
    \frametitle{Limitations}
    These results are hardly generalizable:
    \begin{itemize}
      \item Small project
      \item Single developer
    \end{itemize}
  \end{frame}
}

\section{Conclusions}
\label{sec:conclusion}

This paper presented a case study on structural complexity evolution.
We analyzed 21 versions of the Ristretto project, and concluded it grows
consistently, and its structural complexity increases as time passes. Both
size and complexity metrics have a high correlation with the release
day.
%
We identified that
specific releases where structural complexity decreases or starts to increase
more slowly than in the previous release seem to be related to
significant architectural changes. These changes, in the case of this
small project, were additions of new modules.

We believe that our approach can be used to make larger-scale studies.
These include individual studies with projects larger than Ristretto and
studies comparing the structural complexity evolution of different
projects. By comparing several different projects, perhaps we'll be able
to associate different patterns of structural complexity evolution with
characteristics of the projects. In special, it would be interesting to
compare the results of studying C projects with the results of the Java
projects studied by Stewart et al \cite{stewart-et-al-2006}. Ristretto,
as this paper has shown, seems to belong to the \emph{early increasers}
group described by them. Other type of study that may produce interesting results is
studying structural complexity evolution in a more fine-grained scale:
instead of analyzing only the released source code, we can use the history stored in
the version control system and analyze every single revision to
identify the exact changes that introduced either an increase in
complexity or a refactoring that made the software less complex.

The approach used in this paper can also be used by Free Software
developers to monitor the structural complexity of their C projects. By
using the \texttt{egypt} tool to obtain both design graphs and metrics,
they can verify whether a specific change increases the overall system
complexity or if a refactoring reduced the complexity in
comparison with a given previous state of the code. They can also
inspect the history of the projects in points in which the complexity
decreased to learn important lessons about their own projects.

\mode<presentation>{
\begin{frame}
  \frametitle{Closing remarks}
  \begin{itemize}
    \item Ristretto exhibits consistent growth over time and \emph{punctuated
      equilibrium}-style evolution in structural complexity.
    \item Approach used can be easily used by Free Software developers
      in general:
      \begin{itemize}
        \item to compare different versions of the project according to
          the modularity metrics we used (e.g. to choose the variation
          of a patch that introduces less complexity).
        \item to inspect history and learn lessons.
      \end{itemize} 
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Future work}
  \begin{itemize}
    \item Larger scale study.
    \item Compare with the Java projects studied by
      \cite{stewart-et-al-2006}
    \item more fine-grained analysis of structural complexity evolution
      over time.
  \end{itemize}
\end{frame}
}

\begin{frame}
  \frametitle<presentation>{References}
  \mode<presentation>{\tiny}
  \bibliographystyle{apalike}
  \bibliography{references}
\end{frame}

\mode<presentation>{
  \begin{frame}
    \frametitle{Questions?}

    \begin{center}
      Antonio Terceiro \\
      \texttt{terceiro@dcc.ufba.br}\\
      \url{http://www.dcc.ufba.br/~terceiro/}
    \end{center}
  \end{frame}
}

\end{document}

