NAME = structural-complexity-evolution-case-study
BIBTEXSRCS = references.bib
slides_TEXSRCS = slides.tex structural-complexity-evolution-case-study.tex
slides_BIBTEXSRCS = references.bib
OTHER += svifip.clo svmult.cls
CLEAN_FILES += slides.*

PLOTS = $(wildcard data/*.gpi)
PLOTS_EPS = $(PLOTS:.gpi=.eps)
OTHER += $(PLOTS_EPS)
CLEAN_FILES += $(PLOTS_EPS)

GRAPHS = $(wildcard data/*.dot)
GRAPHS_EPS = $(GRAPHS:.dot=.eps)
OTHER += arch-evolution.eps
CLEAN_FILES += $(GRAPHS_EPS) arch-evolution.eps

include /usr/share/latex-mk/latex.gmk

slides.tex: structural-complexity-evolution-case-study.tex
	sed -e 's/documentclass\[ifip\]{svmult}/\documentclass[ignorenonframetext]{beamer}\n\\usetheme{Warsaw}/; /beamerarticle/d; /\\titlerunning/d; s/\\title\*/\\title/' $< > $@

$(PLOTS_EPS): data/%.eps : data/%.gpi
	cd data && gnuplot $(patsubst data/%,%,$<)

$(GRAPHS_EPS): data/%.eps : data/%.dot
	dot -Teps -o $@ $<

arch-evolution.eps: data/0.0.1.eps data/0.0.6.eps data/0.0.16.eps data/0.0.21.eps
	montage -geometry 400x400 -tile 4x1 -pointsize 36 $(shell echo $^ | sed -e 's#data/\([0-9]\+\.[0-9]\+\.[0-9]\+\)\.eps#-label \1 data/\1.eps#g' ) $@

clean::
	@echo "it's clean"
